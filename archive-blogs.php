<?php
get_header(); ?>
    <div class="container-fluid body_class">
        <div class="container spacing">
            <?php if (false === ($blog_transient = get_transient('blog_transient'))) : ?>
                <?php ob_start(); ?>
                <div class="col-md-8"><!-- Main Content -->
                    <div class="row">
                        <?php
                        $args = array(
                            'post_type'         => 'blogs',
                            'posts_per_page'    => -1,
                            'orderby'           => 'date'
                        );
                        $the_query = new WP_Query( $args );
                        if ( $the_query->have_posts() ) :
                            while ( $the_query->have_posts() ) : $the_query->the_post();
                                ?>
                                <div class="col-md-12 main">
                                    <a href="<?php the_permalink(); ?>">
                                        <div class="thumbnail-testimonial">
                                            <?php $featuredImage = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' ); ?>
                                            <img src="<?php echo $featuredImage[0]; ?>" class="img-responsive" alt="" />
                                            <h2><?php the_title(); ?></h2>
                                            <?php
                                                if (has_excerpt( $post->ID )) {
                                                    the_excerpt();
                                                } else {
                                                    echo '<p>'.get_first_paragraph(get_the_content( $post->ID )).'</p>';
                                                }
                                            ?>
                                            <div class="clearfix"></div>
                                        </div>
                                    </a>
                                </div>
                            <?php
                            endwhile; endif; wp_reset_postdata();
                        ?>
                    </div><!-- End row -->
                </div><!-- End Main Content -->
                <?php
                // Save content as a variable
                $blog_transient = ob_get_contents();
                //empty buffer
                ob_end_clean();
                // save the transient for the last 12 hours
                set_transient('blog_transient', $blog_transient, 12 * HOUR_IN_SECONDS);
            endif; ?>
            <?php echo $blog_transient; ?>
            <?php get_sidebar(); ?>
        </div>
    </div>
<?php get_footer(); ?>