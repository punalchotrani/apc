<?php get_header(); ?>
  <!-- Content Row -->
  <div class="container-fluid body_class">
    <?php include 'inc/slider.php'; ?>
    <div class="container spacing">
      <div class="col-md-8"><!-- Main Content -->
        <div class="row">
					<div class="thumbnail breather">
						<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	 						<h1><?php the_title(); ?></h1>
							<hr>
	 						<?php the_content(); ?>
							<?php the_post_thumbnail(); ?>
							<?php endwhile; ?>
						<?php endif; ?>
					</div>
        </div><!-- /row -->
      </div><!-- End Main Content -->
    <?php get_sidebar(); ?>
  </div>
</div>
<?php get_footer(); ?>
