<?php
get_header(); ?>
<div class="container-fluid body_class">
  <div class="row"><!--Jumbotron row -->
    <?php
      $args = array( 'post_type' => 'slides', 'category_name' => 'offers', 'posts_per_page' => -1, "order" => "ASC", "orderby" => "menu_order");
      $query = new WP_Query( $args );
      $cc = count($query);
      $caption = $attachment->post_excerpt;
      if ( $query->have_posts() ) {
        $i=0;
        while ( $query->have_posts() ) {
          $query->the_post();
    ?>
    <div id="carousel-example-generic" class="carousel slide carousel-fade" data-ride="carousel">
      <ol class="carousel-indicators"><!-- Indicators -->
        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
        <li data-target="#carousel-example-generic" data-slide-to="2"></li>
      </ol>
      <div class="carousel-inner"><!-- Wrapper for slides -->
        <div class="item <?php echo ($i==0)?'active':'' ?>">
          <img src="<?php echo $featuredImage[0]; ?>" alt="" />">
          <div class="carousel-caption">
            <h3><?php echo $caption; ?></h3>
          </div>
        </div>
        <?php
            $i++;
          }
        } wp_reset_query();
        ?>
      </div>
    </div> <!-- Carousel -->
  </div><!--END Jumbotron row -->
  <div class="container spacing">
    <div class="col-md-8"><!-- Main Content -->
      <div class="row">
        <div class="panel-group" id="accordion">
          <?php
            $args = array( 'post_type' => 'faq', 'posts_per_page' => -1, 'order' => 'ASC', "orderby" => "menu_order");
            $the_query = new WP_Query( $args );
              // The Loop
            if ( $the_query->have_posts() ) :
              $x = 1;
              while ( $x <= $the_query->have_posts() ) : $the_query->the_post(); ?>
              <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $x; ?>"><?php the_title(); ?></a>
                    </h4>
                </div>
                <div id="collapse<?php echo $x; ?>" class="panel-collapse collapse">
                    <div class="panel-body">
                        <p><?php the_content(); ?></p>
                    </div>
                </div>
              </div>
            <?php $x++; endwhile; endif; wp_reset_postdata(); ?>
        </div>
      </div><!-- End row -->
    </div><!-- End Main Content -->
  <?php get_sidebar(); ?>
  </div>
</div>
<?php get_footer();?>
