<?php
/*
Template Name: contact us
*/
//include 'inc/contact-form-script.php';
get_header(); ?>
<div class="container-fluid body_class">
  <?php include 'inc/slider.php'; ?>
  <div class="container spacing">
    <div class="col-md-8"><!-- Main Content -->
      <div class="well">
        <!-- <form action="" method="POST">
          <div class="panel-body">
            <div class="form-group">
              <input type="text" name="InputName" placeholder="Name" class="form-control" autofocus="autofocus" required>
            </div>
            <div class="form-group">
              <input type="email" name="InputEmail" placeholder="Email" class="form-control" required>
            </div>
            <div class="form-group">
              <input type="number" name="InputCno" placeholder="Phone" class="form-control" required>
            </div>
            <div class="form-group">
              <textarea name="InputMessage" rows="20" class="form-control" type="text" required></textarea>
            </div>
            <div class="">
              <button type="submit" class="btn btn-info pull-right">Send <span class="glyphicon glyphicon-send"></span></button>
              <button type="reset" value="Reset" name="reset" class="btn">Reset <span class="glyphicon glyphicon-refresh"></span></button>
            </div>
          </div>
        </form> -->
        <?php echo do_shortcode( '[contact-form-7 id="1834" title="Contact form 1"]' ); ?>

      </div><!-- End Well Content -->
    </div><!-- End Main Content -->
  <?php get_sidebar(); ?>
</div>
</div>
<?php get_footer();?>
