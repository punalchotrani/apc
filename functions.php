<?php
/* Enqueuing Styles*/
function enqueue_apc_styles() {
wp_enqueue_style('bootstrap',  get_template_directory_uri() .  '/css/bootstrap/css/bootstrap.min.css');
wp_enqueue_style('normalize',  get_template_directory_uri() .  '/css/normalize.css');
wp_enqueue_style( 'google-fonts', 'https://fonts.googleapis.com/css?family=Cabin:400,400i,500,500i,600,600i,700,700i', false );
wp_enqueue_style( 'font-awesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css', false );
wp_enqueue_style('main',  get_template_directory_uri() . '/builds/main.min.css');
}
add_action('wp_enqueue_scripts', 'enqueue_apc_styles');

function enqueue_apc_scripts() {
  wp_deregister_script( 'jquery' );
  wp_register_script( 'jquery', 'http://code.jquery.com/jquery-2.2.4.min.js', false, '1.9.1', true);
  wp_register_script( 'bootstrap-js', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js', array('jquery'), '3.3.6',true);
  wp_register_script( 'script', get_template_directory_uri() . '/builds/scripts.min.js', array('jquery'), '1.0', true);
  wp_register_script( 'analytics', get_template_directory_uri() . '/analytics/analytics.js','','1.0', true);
  wp_enqueue_script( 'jquery' );
  wp_enqueue_script( 'bootstrap-js' );
  wp_enqueue_script( 'script' );
  wp_enqueue_script( 'analytics' );
}
add_action( 'wp_enqueue_scripts', 'enqueue_apc_scripts' );

//Adding support for fetured images to theme
add_theme_support( 'post-thumbnails' );
//add_theme_support( 'post-formats', array( 'video' ) );
//set_post_thumbnail_size( 1000, 500 );
add_image_size( 'apc-medium', 500, 300, array( 'center', 'center' ) );
add_image_size( 'carousel-size', 750, 410, array('center', 'center') );

add_action( 'admin_init', 'posts_order_wpse_91866' );

function posts_order_wpse_91866()
{
    add_post_type_support( 'post', 'page-attributes' );
}


// Includes
include 'functions/custom-post-type.php';
include 'functions/custom-fields.php';


function first_sentence($content) {
    $pos = strpos($content, '.');
    return substr($content, 0, $pos+1);
}

function WPTime_add_custom_class_to_all_images($content){
    /* Filter by Qassim Hassan - http://wp-time.com */
    $my_custom_class = "img-responsive"; // your custom class
    $add_class = str_replace('<img class="', '<img class="'.$my_custom_class.' ', $content); // add class
    return $add_class; // display class to image
}
add_filter('the_content', 'WPTime_add_custom_class_to_all_images');


add_shortcode( 'wp_caption', 'fixed_img_caption_shortcode' );
add_shortcode( 'caption', 'fixed_img_caption_shortcode' );

function fixed_img_caption_shortcode($attr, $content = null) {
	 if ( ! isset( $attr['caption'] ) ) {
		 if ( preg_match( '#((?:<a [^>]+>s*)?<img [^>]+>(?:s*</a>)?)(.*)#is', $content, $matches ) ) {
		 $content = $matches[1];
		 $attr['caption'] = trim( $matches[2] );
		 }
	 }
	 $output = apply_filters( 'img_caption_shortcode', '', $attr, $content );
		 if ( $output != '' )
		 return $output;
	 extract( shortcode_atts(array(
	 'id'      => '',
	 'align'   => 'alignnone',
	 'width'   => '',
	 'caption' => ''
	 ), $attr));
	 if ( 1 > (int) $width || empty($caption) )
	 return $content;
	 if ( $id ) $id = 'id="' . esc_attr($id) . '" ';
	 return '<div ' . $id . 'class="wp-caption ' . esc_attr($align) . '" >'
	 . do_shortcode( $content ) . '<p class="wp-caption-text">' . $caption . '</p></div>';
}

//Navigation setup
add_action( 'after_setup_theme', 'wpt_setup' );
    if ( ! function_exists( 'wpt_setup' ) ):
        function wpt_setup() {
            register_nav_menu( 'primary', __( 'Primary navigation', 'apc-nav' ) );
            register_nav_menu( 'secondary', __( 'Secondary navigation', 'apc-nav' ) );
        } endif;
require_once('wp_bootstrap_navwalker.php');

//Adding a class to tables
function custom_table_class( $content ) {
    return str_replace( '<table>', '<table class="table">', $content );
}
add_filter( 'the_content', 'custom_table_class' );


/**
 * Wrap videos embedded via oEmbed to make them responsive
 */
function p2_wrap_oembed( $html, $url, $attr, $post_id ) {
	return '<div class="video-embed">' . $html . '</div>';
}
add_filter( 'embed_oembed_html', 'p2_wrap_oembed', 99, 4 );

/**
 * Wrap videos embedded via <iframe> or <embed> to make them responsive as well
 */
function p2_wrap_iframe( $content ) {
    // Match any iframes or embeds
    $pattern = '~<iframe.*</iframe>|<embed.*</embed>~';
    preg_match_all( $pattern, $content, $matches );
    foreach ( $matches[0] as $match ) {
        $wrappedframe = '<div class="video-embed">' . $match . '</div>';
        $content = str_replace($match, $wrappedframe, $content);
    }
    return $content;
}
add_filter( 'the_content', 'p2_wrap_iframe' );


// Customize mce editor font sizes
if ( ! function_exists( 'wpex_mce_text_sizes' ) ) {
	function wpex_mce_text_sizes( $initArray ){
		$initArray['fontsize_formats'] = "9px 10px 12px 13px 14px 16px 18px 21px 24px 28px 32px 36px 42px 55px 62px 68px 72px 78px 82px 88px 95px 98px 102px 110px 118px";
		return $initArray;
	}
}
add_filter( 'tiny_mce_before_init', 'wpex_mce_text_sizes' );


//Defer parsing of JS
function defer_parsing_of_js ( $url ) {
    if ( FALSE === strpos( $url, '.js' ) ) return $url;
    if ( strpos( $url, 'jquery.js' ) ) return $url;
    return "$url' defer='defer";
}
add_filter( 'clean_url', 'defer_parsing_of_js', 11, 1 );


//Delete Transient function
function purge_project_transient(  ) {
        delete_transient( 'main_content_transient' );
        delete_transient( 'blog_transient' );
        delete_transient( 'services_transient' );
        delete_transient( 'testimonial_transient' );
        delete_transient( 'sidebar_transient' );
}
add_action( 'publish_post', 'purge_project_transient' );
add_action( 'save_post', 'purge_project_transient' );
add_action( 'delete_post', 'purge_project_transient' );

//Get the first paragraph of the post
function get_first_paragraph(){
    global $post;

    $str = wpautop( get_the_content() );
    $str = substr( $str, 0, strpos( $str, '</p>' ) + 4 );
    $str = strip_tags($str, '<a><strong><em>');
    return '<p>' . $str . '</p>';
}