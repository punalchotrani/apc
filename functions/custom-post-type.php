<?php

// Register Custom Post Type
function adding_post_type()
{
    //Testimonials-----------------------------------------//
    register_post_type('Testimonials',
        // CPT Options
        array(
            'labels' => array(
                'name' => __('Testimonials'),
                'singular_name' => __('Testimonial')
            ),
            'public' => true,
            'menu_position' => 4,
            'has_archive' => true,
            'menu_icon' => 'dashicons-groups',
            'rewrite' => array('slug' => 'testimonials'),
        )
    );
    //Testimonials-----------------------------------------//


    //Slides-----------------------------------------//
    $labels = array(
        'name' => _x('Slides', 'Post Type General Name', 'text_domain'),
        'singular_name' => _x('Slide', 'Post Type Singular Name', 'text_domain'),
        'menu_name' => __('Slides', 'text_domain'),
        'name_admin_bar' => __('Slides', 'text_domain'),
        'archives' => __('Item Archives', 'text_domain'),
        'parent_item_colon' => __('Parent Item:', 'text_domain'),
        'all_items' => __('All Slides', 'text_domain'),
        'add_new_item' => __('Add New Slide', 'text_domain'),
        'add_new' => __('Add New Slide', 'text_domain'),
        'new_item' => __('New Slide', 'text_domain'),
        'edit_item' => __('Edit Slider', 'text_domain'),
        'update_item' => __('Update Slide', 'text_domain'),
        'view_item' => __('', 'text_domain'),
        'search_items' => __('Search Slides', 'text_domain'),
        'not_found' => __('No Slide Found', 'text_domain'),
        'not_found_in_trash' => __('Not found in Trash', 'text_domain'),
        'featured_image' => __('Slider Image', 'text_domain'),
        'set_featured_image' => __('Set Slider image', 'text_domain'),
        'remove_featured_image' => __('Remove slider image', 'text_domain'),
        'use_featured_image' => __('Use as slide image', 'text_domain'),
        'insert_into_item' => __('Insert into item', 'text_domain'),
        'uploaded_to_this_item' => __('Uploaded to this item', 'text_domain'),
        'items_list' => __('Items list', 'text_domain'),
        'items_list_navigation' => __('Items list navigation', 'text_domain'),
        'filter_items_list' => __('Filter items list', 'text_domain'),
    );
    $args = array(
        'label' => __('Slide', 'text_domain'),
        'description' => __('Add images to the site\'s slider', 'text_domain'),
        'labels' => $labels,
        'supports' => array('title', 'thumbnail', 'editor'),
        'hierarchical' => false,
        'taxonomies' => array('category'),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 5,
        'menu_icon' => 'dashicons-images-alt2',
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'rewrite' => false,
        'capability_type' => 'page',
    );
    register_post_type('slider', $args);
    //Slides-----------------------------------------//

    //Services-----------------------------------------//
    $labels = array(
        'name' => _x('Services', 'Post Type General Name', 'text_domain'),
        'singular_name' => _x('Service', 'Post Type Singular Name', 'text_domain'),
        'menu_name' => __('Services', 'text_domain'),
        'name_admin_bar' => __('Services', 'text_domain'),
        'archives' => __('Item Archives', 'text_domain'),
        'parent_item_colon' => __('Parent Item:', 'text_domain'),
        'all_items' => __('All Services', 'text_domain'),
        'add_new_item' => __('Add New Service', 'text_domain'),
        'add_new' => __('Add New Service', 'text_domain'),
        'new_item' => __('New Service', 'text_domain'),
        'edit_item' => __('Edit Service', 'text_domain'),
        'update_item' => __('Update Service', 'text_domain'),
        'view_item' => __('', 'text_domain'),
        'search_items' => __('Search Services', 'text_domain'),
        'not_found' => __('No Service Found', 'text_domain'),
        'not_found_in_trash' => __('Not found in Trash', 'text_domain'),
        'featured_image' => __('Featured Image', 'text_domain'),
        'set_featured_image' => __('Set Service image', 'text_domain'),
        'remove_featured_image' => __('Remove service image', 'text_domain'),
        'use_featured_image' => __('Use as service image', 'text_domain'),
        'insert_into_item' => __('Insert into item', 'text_domain'),
        'uploaded_to_this_item' => __('Uploaded to this item', 'text_domain'),
        'items_list' => __('Items list', 'text_domain'),
        'items_list_navigation' => __('Items list navigation', 'text_domain'),
        'filter_items_list' => __('Filter items list', 'text_domain'),
    );
    $args = array(
        'label' => __('Service', 'text_domain'),
        'description' => __('Add services', 'text_domain'),
        'labels' => $labels,
        'supports' => array('title', 'editor', 'thumbnail', 'page-attributes'),
        'hierarchical' => true,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 3,
        'menu_icon' => 'dashicons-money',
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'rewrite' => array('slug' => 'services'),
        'capability_type' => 'post',
    );
    register_post_type('service', $args);
    //Services-----------------------------------------//

    //Offers-----------------------------------------//

    $labels = array(
        'name' => _x('Offers', 'Post Type General Name', 'text_domain'),
        'singular_name' => _x('Offer', 'Post Type Singular Name', 'text_domain'),
        'menu_name' => __('Offers', 'text_domain'),
        'name_admin_bar' => __('Offer', 'text_domain'),
        'archives' => __('Offer Archives', 'text_domain'),
        'parent_item_colon' => __('Parent Offer:', 'text_domain'),
        'all_items' => __('All Offers', 'text_domain'),
        'add_new_item' => __('Add New Offer', 'text_domain'),
        'add_new' => __('Add Offer', 'text_domain'),
        'new_item' => __('New Offer', 'text_domain'),
        'edit_item' => __('Edit Offer', 'text_domain'),
        'update_item' => __('Update Offer', 'text_domain'),
        'view_item' => __('View Offer', 'text_domain'),
        'search_items' => __('Search Offer', 'text_domain'),
        'not_found' => __('Not found', 'text_domain'),
        'not_found_in_trash' => __('Not found in Trash', 'text_domain'),
        'featured_image' => __('Offer Image', 'text_domain'),
        'set_featured_image' => __('Set offer image', 'text_domain'),
        'remove_featured_image' => __('Remove offer image', 'text_domain'),
        'use_featured_image' => __('Use as offer image', 'text_domain'),
        'insert_into_item' => __('Insert into offer', 'text_domain'),
        'uploaded_to_this_item' => __('Uploaded to this offer', 'text_domain'),
        'items_list' => __('Offers list', 'text_domain'),
        'items_list_navigation' => __('Offers list navigation', 'text_domain'),
        'filter_items_list' => __('Filter offers list', 'text_domain'),
    );
    $args = array(
        'label' => __('Offer', 'text_domain'),
        'description' => __('Offers Section', 'text_domain'),
        'labels' => $labels,
        'supports' => array('title', 'editor', 'thumbnail', 'post-formats',),
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 10,
        'menu_icon' => 'dashicons-tickets',
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'capability_type' => 'page',
    );
    register_post_type('offers', $args);
    //Offers-----------------------------------------//


    //FAQ-----------------------------------------//
    $labels = array(
        'name' => __('FAQS', 'faq'),
        'singular_name' => __('FAQ', 'faq'),
        'add_new' => __('Add New FAQ', 'faq'),
        'add_new_item' => __('Add New FAQ', 'faq'),
        'edit_item' => __('Edit FAQ', 'faq'),
        'new_item' => __('New FAQ', 'faq'),
        'view_item' => __('View FAQ', 'faq'),
        'search_items' => __('Search FAQ', 'faq'),
        'not_found' => __('No FAQ found', 'faq'),
        'not_found_in_trash' => __('No FAQ found in Trash', 'faq'),
        'parent_item_colon' => __('Parent FAQ:', 'faq'),
        'menu_name' => __('FAQS', 'faq'),
    );

    $args = array(
        'labels' => $labels,
        'hierarchical' => false,
        'description' => 'This is the Frequently asked questions.',
        'supports' => array('title', 'editor'),
        'taxonomies' => array('category'),
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 10,
        'menu_icon' => 'dashicons-pressthis',
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'capability_type' => 'page',
    );

    register_post_type('faq', $args);
    //FAQ-----------------------------------------//


    //News-----------------------------------------//
    $labels = array(
        'name' => __('NEWS', 'news'),
        'singular_name' => __('NEWS', 'news'),
        'add_new' => __('Add New NEWS Item', 'news'),
        'add_new_item' => __('Add New NEWS Item', 'news'),
        'edit_item' => __('Edit NEWS Item', 'news'),
        'new_item' => __('New NEWS Item', 'news'),
        'view_item' => __('View NEWS Item', 'news'),
        'search_items' => __('Search NEWS Item', 'news'),
        'not_found' => __('No NEWS Item found', 'news'),
        'not_found_in_trash' => __('No NEWS Item found in Trash', 'news'),
        'parent_item_colon' => __('Parent NEWS Item:', 'news'),
        'menu_name' => __('News', 'news'),
    );

    $args = array(
        'labels' => $labels,
        'hierarchical' => false,
        'description' => 'This is the News.',
        'supports' => array('title', 'editor'),
        'taxonomies' => array('category'),
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 10,
        'menu_icon' => 'dashicons-feedback',
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'capability_type' => 'page',
    );

    register_post_type('news', $args);
    //News-----------------------------------------//

    //Blogs-----------------------------------------//
    register_post_type('Blogs',
        // CPT Options
        array(
            'labels' => array(
                'name' => __('Blogs'),
                'singular_name' => __('Blog')
            ),
            'public' => true,
            'menu_position' => 5,
            'has_archive' => true,
            'menu_icon' => 'dashicons-welcome-write-blog',
            'rewrite' => array('slug' => 'blogs'),
            'supports' => array('title', 'thumbnail', 'editor', 'excerpt'),
            'taxonomies' => array('category','post_tag')
        )
    );
    //Blogs-----------------------------------------//
}

add_action('init', 'adding_post_type', 0);


function my_rewrite_flush()
{
    // First, we "add" the custom post type via the above written function.
    // Note: "add" is written with quotes, as CPTs don't get added to the DB,
    // They are only referenced in the post_type column with a post entry,
    // when you add a post of this CPT.
    adding_post_type();

    // ATTENTION: This is *only* done during plugin activation hook in this example!
    // You should *NEVER EVER* do this on every page load!!
    flush_rewrite_rules();
}

register_activation_hook(__FILE__, 'my_rewrite_flush');
