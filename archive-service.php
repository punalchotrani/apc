<?php
get_header(); ?>
<div class="container-fluid body_class">
  <div class="container spacing">
    <?php if (false === ($services_transient = get_transient('services_transient'))) : ?>
        <?php ob_start(); ?>
        <div class="col-md-8"><!-- Main Content -->
          <div class="row">
            <h1 class="visuallyHidden">Cleaning services in Birmingham</h1>
            <?php
              $args = array(
                  'post_type'         => 'service',
                  'posts_per_page'    => -1,
                  'order'             => 'ASC',
                  'orderby'           => 'menu_order'
              );
              $the_query = new WP_Query( $args );
              $counter = 0;
              if ( $the_query->have_posts() ) :
                while ( $the_query->have_posts() ) : $the_query->the_post();
            ?>
            <div class="col-md-4 main spacing-bottom">
              <div class="thumbnail">
                <?php $specialOffer = get_post_meta($post->ID, 'my_meta_box_check', true);
                if ( $specialOffer == 'on') : ?>
                <img src="<?php echo site_url(); ?>/wp-content/themes/apc/img/special_offer.png" class="img-responsive offer" alt="" />
                <?php endif ?>
                <a href="<?php echo get_post_permalink(); ?>" title="<?php the_title(); ?>">
                  <?php $featuredImage = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'apc-medium' ); ?>
                  <img src="<?php echo $featuredImage[0]; ?>" class="img-responsive" alt="" />
                  <div class="caption services-caption">
                    <h2 class="home-h2 text-center"><?php the_title(); ?></h2>
                    <?php
                      $content = get_the_content();
                      echo '<p>'.sanitize_text_field(first_sentence($content)).'</p>';
                    ?>
                  </div>
                </a>
              </div>
              <div class="button-link">
                <a href="<?php echo get_post_permalink(); ?>" title="<?php the_title(); ?>">
                  <div class="first">
                    <p>Find out more<p>
                  </div>
                </a>
                <a href="#">
                  <div class="second">
                    <p>Contact us<p>
                  </div>
                </a>
              </div>
            </div>
            <?php
            $counter++;
                    if ($counter % 3 == 0) {
                    echo '</div><div class="row">';
                  }
                  endwhile; endif; wp_reset_postdata();
            ?>
          </div><!-- End row -->
        </div><!-- End Main Content -->
      <?php
        // Save content as a variable
        $services_transient = ob_get_contents();
        //empty buffer
        ob_end_clean();
        // save the transient for the last 12 hours
        set_transient('services_transient', $services_transient, 12 * HOUR_IN_SECONDS);
      endif; ?>
      <?php echo $services_transient; ?>
  <?php get_sidebar(); ?>
</div>
</div>
<?php get_footer();?>
