<?php get_header(); ?>
<!-- Content Row -->
<div class="container-fluid body_class">
    <?php include 'inc/slider.php'; ?>
    <?php if (false === ($main_content_transient = get_transient('main_content_transient'))) : ?>
        <?php ob_start(); ?>
        <div class="container-fluid">
            <div class="container spacing">
                <div class="col-md-12"><!-- Main Content -->
                    <div class="row">
                        <?php
                        $counter = 0;
                        $args = array('category_name' => 'home-page', 'posts_per_page' => 6, 'orderby' => 'menu_order', 'order' => 'ASC');
                        $the_query = new WP_Query($args);
                        if ($the_query->have_posts()) :
                            while ($the_query->have_posts()) : $the_query->the_post(); ?>
                                <div class="col-md-6 main">
                                    <div class="thumbnail">
                                        <?php $featuredImage = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'apc-medium'); ?>
                                        <?php if (has_post_thumbnail()) : ?>
                                            <img src="<?php echo $featuredImage[0]; ?>"
                                                 class="img-responsive full-width" alt=""/>
                                        <?php endif; ?>
                                        <div class="caption">
                                            <h3 class="home-h3 text-center"><?php the_title(); ?></h3>
                                            <?php the_content(); ?>
                                        </div>
                                    </div>
                                </div>
                                <?php $counter++;
                                if ($counter % 2 == 0) {
                                    echo '</div><div class="row">';
                                }
                            endwhile; endif;
                        wp_reset_postdata(); ?>
                    </div><!-- /row -->
                </div><!-- End Main Content -->
            </div>
        </div>
        <?php
        // Save content as a variable
        $main_content_transient = ob_get_contents();
        //empty buffer
        ob_end_clean();
        // save the transient for the last 12 hours
        set_transient('main_content_transient', $main_content_transient, 12 * HOUR_IN_SECONDS);
    endif; ?>
    <?php echo $main_content_transient; ?>
</div>
<div class="container-fluid home-testimonial">
    <div class="container">
        <?php if (false === ($home_testimonial_transient = get_transient('home_testimonial_transient'))) : ?>
        <?php ob_start(); ?>
        <div class="row"><!-- Row Testimonial -->
            <?php
            $args = array('post_type' => 'testimonials', 'posts_per_page' => 2);
            $the_query = new WP_Query($args);
            if ($the_query->have_posts()) :
                while ($the_query->have_posts()) : $the_query->the_post();
                    ?>
                    <div class="col-md-12">
                        <blockquote class="bq3">
                            <?php the_content(); ?>
                            <cite><?php the_title(); ?></cite>
                        </blockquote>
                    </div>
                <?php endwhile;
            endif;
            wp_reset_postdata(); ?>
        </div><!-- End Row Testimonial -->
        <?php if (wp_count_posts('testimonials')->publish > 3) : ?>
            <a href="<?php echo get_home_url(); ?>/testimonials" class="btn btn-hero btn-lg pull-right">More
                testimonials</a>
        <?php endif; ?>
    </div>
    <?php // Save content as a variable
    $home_testimonial_transient = ob_get_contents();
    //empty buffer
    ob_end_clean();
    // save the transient for the last 12 hours
    set_transient('home_testimonial_transient', $home_testimonial_transient, 12 * HOUR_IN_SECONDS);
    endif; ?>
    <?php echo $home_testimonial_transient; ?>
</div>
<?php
$args = array('post_type' => 'news', 'posts_per_page' => 1);
$the_query = new WP_Query($args);
if ($the_query->have_posts()) : ?>
    <div class="container-fluid home-news">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>News</h2>
                    <hr>
                    <div class="caption">
                        <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
                            <?php the_content(); ?>
                        <?php endwhile; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif;
wp_reset_postdata(); ?>
<?php get_footer(); ?>
