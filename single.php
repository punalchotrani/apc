<?php get_header(); ?>
  <!-- Content Row -->
  <div class="container-fluid body_class">
   <?php if ( has_post_thumbnail() ) : ?>
    <div class="row carousel-row ssup"><!--Jumbotron row -->
      <div class="carousel-inner">
        <div class="carousel fade-carousel slide" style="background-image: url(<?php echo wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' )[0] ?>)"></div>
      </div>
    </div><!--END Jumbotron row -->
   <?php endif; ?>
    <div class="container spacing">
      <div class="col-md-8"><!-- Main Content -->
        <div class="row">
					<div class="thumbnail breather">
						<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	 						<h1><?php the_title(); ?></h1>
							<hr>
	 						<?php the_content(); ?>
							<?php endwhile; ?>
						<?php endif; ?>
                        <div class="clearfix"></div>
					</div>
        </div><!-- /row -->
      </div><!-- End Main Content -->
    <?php get_sidebar(); ?>
  </div>
</div>
<?php get_footer(); ?>
