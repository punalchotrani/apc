$(window).load(function () {
    $(window).on("load, resize", function () {
        var viewportWidth = $(window).width();
        if (viewportWidth < 992) {
            $(".logo").addClass("center-block");
        }
        else {
            $(".logo").removeClass("center-block");
        }
    });
    //set the starting bigestHeight variable
    var biggestHeight = 0;
    //check each of them
    $('.main > .thumbnail').each(function () {
        //if the height of the current element is
        //bigger then the current biggestHeight value
        if ($(this).height() > biggestHeight) {
            //update the biggestHeight with the
            //height of the current elements
            biggestHeight = $(this).height();
        }
    });
    //when checking for biggestHeight is done set that
    //height to all the elements
    if ($(window).width() > 960) {
        $('.main > .thumbnail').height(biggestHeight);
    }
    $('#bs-carousel').carousel({
        interval: 5000
    });

    $('p').each(function () {
        var $this = $(this);
        if ($this.html().replace(/\s|&nbsp;/g, '').length == 0)
            $this.remove();
    });

    $(".navbar-toggle").on("click", function () {
        $('.navbar-menubuilder').toggle();
    });

});
