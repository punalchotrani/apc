<?php
get_header(); ?>
<div class="container-fluid body_class">
  <div class="row"><!--Jumbotron row -->
    <?php
      $args = array( 'post_type' => 'slides', 'category_name' => 'offers', 'posts_per_page' => -1, "order" => "ASC", "orderby" => "menu_order");
      $query = new WP_Query( $args );
      $cc = count($query);
      //$caption = $attachment->post_excerpt;
      if ( $query->have_posts() ) {
        $i=0;
        while ( $query->have_posts() ) {
          $query->the_post();
    ?>
    <div id="carousel-example-generic" class="carousel slide carousel-fade" data-ride="carousel">
      <ol class="carousel-indicators"><!-- Indicators -->
        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
        <li data-target="#carousel-example-generic" data-slide-to="2"></li>
      </ol>
      <div class="carousel-inner"><!-- Wrapper for slides -->
        <div class="item <?php echo ($i==0)?'active':'' ?>">
          <img src="<?php echo $featuredImage[0]; ?>" alt="" />">
          <div class="carousel-caption">
            <h3><?php echo $caption; ?></h3>
          </div>
        </div>
        <?php
            $i++;
          }
        } wp_reset_query();
        ?>
      </div>
    </div> <!-- Carousel -->
  </div><!--END Jumbotron row -->
  <div class="container spacing">
    <div class="col-md-8"><!-- Main Content -->
      <div class="row">
        <?php
          $args = array(
              'post_type'         => 'offers',
              'posts_per_page'    => -1,
              'order'             => 'ASC',
              'orderby'           => 'menu_order'
          );
          $the_query = new WP_Query( $args );
          if ( $the_query->have_posts() ) :
            while ( $the_query->have_posts() ) : $the_query->the_post();
        ?>
        <div class="col-md-12 main offers">
          <?php $featuredImage = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' ); ?>
          <div class="thumbnail-offer" style="background-image: url('<?php echo $featuredImage[0]; ?>'), linear-gradient(to right, #1f8d84, #FFFFE0)">
            <div class="caption">
              <h3 class="text-center"><?php the_title(); ?></h3>
              <?php $content = get_the_content(); ?>
              <p class="text-center"><?php echo $content; ?></p>
            </div>
          </div>
        </div>
        <?php endwhile; endif; wp_reset_postdata(); ?>
      </div><!-- End row -->
    </div><!-- End Main Content -->
  <?php get_sidebar(); ?>
</div>
</div>
<?php get_footer();?>
