<?php
get_header(); ?>
<div class="container-fluid body_class">
    <div class="container spacing">
        <?php if (false === ($testimonial_transient = get_transient('testimonial_transient'))) : ?>
            <?php ob_start(); ?>
            <div class="col-md-8"><!-- Main Content -->
                <div class="row">
                    <?php
                    $args = array(
                        'post_type'         => 'testimonials',
                        'posts_per_page'    => -1,
                        'order'             => 'ASC',
                        'orderby'           => 'menu_order'
                    );
                    $the_query = new WP_Query( $args );
                    if ( $the_query->have_posts() ) :
                        while ( $the_query->have_posts() ) : $the_query->the_post();
                            ?>
                            <div class="col-md-12 main">
                                <div class="thumbnail-testimonial">
                                    <?php $featuredImage = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'apc-medium' ); ?>
                                    <img src="<?php echo $featuredImage[0]; ?>" class="img-responsive" alt="" />
                                    <blockquote class="bq3">
                                        <?php echo get_the_content(); ?>
                                        <cite><?php the_title(); ?></cite>
                                    </blockquote>
                                </div>
                            </div>
                            <?php
                        endwhile; endif; wp_reset_postdata();
                    ?>
                </div><!-- End row -->
            </div><!-- End Main Content -->
            <?php
            // Save content as a variable
            $testimonial_transient = ob_get_contents();
            //empty buffer
            ob_end_clean();
            // save the transient for the last 12 hours
            set_transient('testimonial_transient', $testimonial_transient, 12 * HOUR_IN_SECONDS);
        endif; ?>
        <?php echo $testimonial_transient; ?>
        <?php get_sidebar(); ?>
    </div>
</div>
<?php get_footer();?>
