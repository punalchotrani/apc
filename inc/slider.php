<div class="row carousel-row"><!--Jumbotron row -->
    <?php if (false === ($slider_transient = get_transient('slider_transient'))) : ?>
        <?php ob_start(); ?>
            <?php $number = 0;
            $args = array('post_type' => 'slider', 'post_per_page' => 6, 'orderby' => 'menu_order', 'order' => 'ASC');
            $the_query = new WP_Query($args);
            if ($the_query->have_posts()) : ?>
            <div class="carousel fade-carousel slide" data-ride="carousel" id="bs-carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <?php $active = false; ?>
                    <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
                        <li data-target="#bs-carousel" data-slide-to="<?php echo $number++; ?>"
                            class="<?php echo !$active ? "active" : ""; ?>"></li>
                        <?php $active = true; ?>
                    <?php endwhile ?>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner">
                    <?php $active = false; ?>
                    <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
                        <div class="item slides <?php echo !$active ? "active" : ""; ?>">
                            <?php
                            $image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full');
                            $src = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'carousel-size', false, '');
                            $thumb_id = get_post_thumbnail_id($post->id);
                            ?>
                            <div style="<?php if ($image[0] == !null) {
                                echo 'background-image: url(' . $image[0] . ')';
                            } ?>" class="item-slide"></div>
                            <div class="hero">
                                <?php the_content(); ?>
                            </div>
                        </div>
                        <?php $active = true; ?>
                    <?php endwhile; ?>
                    <?php endif;
                    wp_reset_query();
                    // Save content as a variable
                    $slider_transient = ob_get_contents();
                    //empty buffer
                    ob_end_clean();
                    // save the transient for the last 12 hours
                    set_transient('slider_transient', $slider_transient, 12 * HOUR_IN_SECONDS);
            endif; ?>
                    <?php echo $slider_transient; ?>
        </div>
    </div> <!-- Carousel -->
</div><!--END Jumbotron row -->
