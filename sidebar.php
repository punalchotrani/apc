<?php if (false === ($sidebar_transient = get_transient('sidebar_transient'))) : ?>
    <?php ob_start(); ?>
    <div class="col-md-4 sidebar"><!-- Sidebar -->
      <div class="thumbnail">
        <h3>Our Services</h3>
        <div class="list-group">
          <?php
            $args = array( 'post_type' => 'service', 'posts_per_page' => -1, 'orderby' => 'menu_order', 'order' => 'ASC');
            $the_query = new WP_Query( $args );
            if ( $the_query->have_posts() ) :
              while ( $the_query->have_posts() ) : $the_query->the_post();
          ?>
            <a href="<?php echo get_post_permalink(); ?>" class="list-group-item"><?php echo get_the_title(); ?></a>
          <?php endwhile; endif; wp_reset_postdata(); ?>
        </div>
      </div>
    </div><!-- End Sidebar -->
<?php
// Save content as a variable
    $sidebar_transient = ob_get_contents();
    //empty buffer
    ob_end_clean();
    // save the transient for the last 12 hours
    set_transient('sidebar_transient', $sidebar_transient, 12 * HOUR_IN_SECONDS);
endif; ?>
<?php echo $sidebar_transient; ?>
