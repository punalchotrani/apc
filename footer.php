  <div class="container-fluid home-testimonial">
      <?php if (false === ($footer_assets_transient = get_transient('footer_assets_transient'))) : ?>
      <?php ob_start(); ?>
        <div class="container">
          <div class="row accreditaion-row">
            <ul class="accreditaion-logos">
                <li>
                  <a href="https://www.bark.com/en/company/all-purpose-cleaning/LJ6K/" target="_blank" class="bark-widget" data-type="reviews" data-id="LJ6K" data-image="medium-gold" data-version="2.0">
                    All Purpose Cleaning
                  </a>
                  <script type="text/javascript" src="https://www.bark.com/js/widgets.min.js" async=""></script>
                </li>
                <li>
                  <a href="http://www.iwestmidlands.co.uk/profile/748668/Birmingham/All-Purpose-Cleaning/">
                    <img src="<?php echo site_url(); ?>/wp-content/themes/apc/img/iWest_Midlands.png" alt="iwest-midlands" title="accreditation-ipaf">
                  </a>
                </li>
                <li>
                  <a href="#">
                    <img src="<?php echo site_url(); ?>/wp-content/themes/apc/img/uka_black.png" alt="uk-acadamy" title="biosweep-accred">
                  </a>
                </li>
                <li>
                  <a href="https://www.bark.com/en/company/all-purpose-cleaning/LJ6K/" data-version="2.0" target="_blank" class="bark-widget" data-image="medium-navy">
                    All Purpose Cleaning
                  </a>
                  <script type="text/javascript" src="https://www.bark.com/js/widgets.min.js" async=""></script>
                </li>
            </ul>
          </div>
        </div>
      </div>
  <div class="container-fluid">
    <div class="container">
      <div class="row">
        <div class="map-responsive">
          <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d1215.6349544620577!2d-1.8392928!3d52.4561391!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4870bbb7a0b2a65d%3A0x87ac66e7d7223517!2sKings+Rd%2C+Birmingham+B11+2AA!5e0!3m2!1sen!2suk!4v1477228183846" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
      </div>
    </div>
  </div>
  <footer>
    <div class="container-fluid">
      <div class="container">
        <div class="row">
          <div class="col-md-3">
            <a href="<?php echo get_home_url(); ?>">
              <img src="<?php bloginfo('template_url') ?>/img/logo.png" alt="logo" class="logo img-responsive" />
            </a>
            <address class="text-center">
              Fairgate House, 205 Kings Road<br>
              Birmingham, B11 2AA<br>
            </address>
          </div>
          <div class="col-md-3">
            <?php /* footer navigation */
              wp_nav_menu( array(
                'menu' => 'Footer Menu',
                'depth' => 5,
                'container' => false,
                'menu_class' => 'list-group',
                //Process nav menu using our custom nav walker
                'walker' => new wp_bootstrap_navwalker())
              );
              ?>
          </div>
          <div class="col-md-3">
            <div class="social-media">
              <ul class="list-group">
                <li><i class="fa fa-facebook fa-2x" aria-hidden="true"></i></li>
                <li><i class="fa fa-twitter fa-2x" aria-hidden="true"></i></li>
                <li><i class="fa fa-youtube fa-2x" aria-hidden="true"></i></li>
              </ul>
            </div>
          </div>
          <div class="col-md-3 text-center">
            <?php
                if ( !is_page('contact-us') ) {
                  echo do_shortcode( '[contact-form-7 id="1920" title="footer contact"]' );
                }
            ?>
          </div>
        </div>
      </div>
    </div>
    <div class="container-fluid terms">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
                <!-- <p>By continuing to use this site you agree to the use of cookies.
For more information and to find out how to change this click here Accept Cookies</p> -->
          </div>
        </div>
      </div>
    </div>
  </footer>
  <?php
  // Save content as a variable
  $footer_assets_transient = ob_get_contents();
  //empty buffer
  ob_end_clean();
  // save the transient for the last 12 hours
  set_transient('footer_assets_transient', $footer_assets_transient, 12 * HOUR_IN_SECONDS);
  endif; ?>
  <?php echo $footer_assets_transient; ?>
    <?php wp_footer(); ?>
</body>
</html>
