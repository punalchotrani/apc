<?php
/*
Template Name: testimonial
*/
get_header(); ?>
<div class="container-fluid body_class">
  <div class="row"><!--Jumbotron row -->
    <div id="carousel-example-generic" class="carousel slide carousel-fade" data-ride="carousel">
      <ol class="carousel-indicators"><!-- Indicators -->
        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
        <li data-target="#carousel-example-generic" data-slide-to="2"></li>
      </ol>
      <div class="carousel-inner"><!-- Wrapper for slides -->
        <div class="item active">
          <img src="http://placehold.it/2200x450" alt="...">
          <div class="carousel-caption">
            <h3>Caption Text</h3>
          </div>
        </div>
        <div class="item">
          <img src="http://placehold.it/2200x450" alt="...">
          <div class="carousel-caption">
            <h3>Caption Text</h3>
          </div>
        </div>
        <div class="item">
          <img src="http://placehold.it/2200x450" alt="...">
          <div class="carousel-caption">
            <h3>Caption Text</h3>
          </div>
        </div>
      </div>
    </div> <!-- Carousel -->
  </div><!--END Jumbotron row -->
  <div class="container spacing">
    <div class="col-md-8 testimonial"><!-- Main Content -->
      <div class="menu row">
          <div class="menu-category list-group ">
            <div class="well">
              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also </p>
            </div>
          </div>
          <div class="menu-category list-group">
            <div class="well">
              <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College</p>
            </div>
          </div>
          <div class="menu-category list-group">
            <div class="well">
              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a</p>
            </div>
          </div>
          <div class="menu-category list-group">
            <div class="well">
              <p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from</p>
            </div>
          </div>
          <div class="menu-category list-group">
            <div class="well">
              <p>here are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum</p>
            </div>
        </div>
        <div class="menu-category list-group">
          <div class="well">
            <p>comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. </p>
          </div>
        </div>
        <div class="menu-category list-group">
          <div class="well">
            <p>five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of L</p>
          </div>
        </div>
      </div>
    </div><!-- End Main Content -->
  <?php get_sidebar(); ?>
</div>
</div>
<?php get_footer();?>
