<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<html lang="en-GB">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title><?php bloginfo('name');wp_title(); ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<!-- Modernizr JS -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>

	<?php wp_head(); ?>
</head>
<body <?php body_class();?>>
	<header>
		<div class="container-fluid">
			<div class="container">
				<div class="row">
					<div class="col-md-9">
						<a href="<?php echo get_home_url(); ?>"><!-- logo goes here -->
							<img src="<?php bloginfo('template_url') ?>/img/logo.png" alt="logo" class="logo" />
						</a>
					</div><!-- End col-md-9 -->
					<div class="col-md-3">
						<address class="text-center"><!-- Address goes here -->
							<!-- <strong class="hidden-sm hidden-xs">All Purpose Cleaning</strong><br /> -->
									<strong><i class="fa fa-phone" aria-hidden="true"></i> 01216678302</strong><br>
									or<br>
									<strong><i class="fa fa-mobile" aria-hidden="true"></i> 07809153697</strong>
						</address>
					</div><!-- End col-md-3 -->
				</div><!-- End row -->
			</div><!-- End container -->
			<div class="row"><!-- Nav Row -->
				<nav class="navbar navbar-default " id="main_menu">
						<div class="navbar-header">
							<button class="navbar-toggle" data-target="navbar-menubuilder" data-toggle="collapse" type="button"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
              </button>
            </div>
						<div class="collapse navbar-collapse navbar-menubuilder">
							<?php /* Primary navigation */
								wp_nav_menu( array (
                                      'menu' => 'Main Menu',
                                      'depth' => 5,
                                      'container' => false,
                                      'menu_class' => 'nav navbar-nav',
                                      //Process nav menu using our custom nav walker
                                      'walker' => new wp_bootstrap_navwalker()
                                  )
								);
								?>
						</div>
				</nav>
			</div><!-- End nav row -->
		</div><!-- End container-fluid -->
	</header>
